import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './components/users/users.component';
import { PokemonsComponent } from './components/pokemons/pokemons.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';

/* PRIME NG */
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';

import { BlockUIModule } from 'ng-block-ui';
import { PokemonDetailsComponent } from './components/pokemons/pokemon-details/pokemon-details.component';
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PokemonsComponent,
    NavbarComponent,
    UserFormComponent,
    PokemonDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    DialogModule,
    MatDialogModule,
    BlockUIModule.forRoot(),
    ToastrModule.forRoot({ closeButton: true }),
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
