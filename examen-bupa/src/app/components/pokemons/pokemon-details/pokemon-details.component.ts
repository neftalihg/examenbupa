import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css']
})
export class PokemonDetailsComponent implements OnInit {

  @Input() openModal: boolean;
  @Input() mode: string;
  @Input() details: any;
  @Output() closed: EventEmitter<any>; 
  
  showModal: boolean;
  modeModal: string;

  currentDetails = [];
  constructor() { 
    this.closed = new EventEmitter();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.showModal = this.openModal;
    this.modeModal = this.mode;
    if(this.details != null && this.modeModal=="Habilidades"){
      this.details.forEach(detail => {
        console.log(detail);
        this.currentDetails.push(detail.ability.name);
      });
    }
    else if(this.details != null && this.modeModal=="Movimientos"){
      this.details.forEach(detail => {
        this.currentDetails.push(detail.move.name);
      }); 
    }
    else{
      this.details = []
    }
    console.log(this.currentDetails);
  }

  ngOnInit(): void {
  }

  close() {
    this.currentDetails = [];
    this.closed.emit();
  }
}
