import { Component, OnInit, ViewChild } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { PokemonsService } from 'src/app/services/pokemons/pokemons.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.css']
})
export class PokemonsComponent implements OnInit {

  totalPages = null;
  Pokemons: Pokemon[] = [];

  @BlockUI() blockUI: NgBlockUI; 

  openModal = false;
  mode = '';

  CurrentPokemonDetails = [];


  //Columnas y DataSource de Tabla
  displayedColumns: string[] = ['name', 'options'];
  dataSource = new MatTableDataSource<Pokemon>(this.Pokemons);

  //Paginador
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private pokemonsService: PokemonsService,
              public dialog: MatDialog,
              private toastr: ToastrService) { }
  async ngOnInit() {
    await this.getPokemons();
    this.dataSource = new MatTableDataSource<Pokemon>(this.Pokemons);
    this.dataSource.paginator = this.paginator;
    console.log(this.Pokemons);
  }

  async getPokemons() {
    await this.pokemonsService.getPokemons()
      .then((data: any) => {
        this.Pokemons = data.results;
      })
      .catch((error) => {
        this.toastr.error('Ocurrió un error al obtener los pokemons');
      });
  }

  async showAbilities(pokemonDetails){
    await this.pokemonsService.getPokemonDetails(pokemonDetails)
      .then((data: any) => {
        this.CurrentPokemonDetails = data.abilities;
      })
      .catch((error) => {
        this.toastr.error('Ocurrió un error al obtener los pokemons');
      });
    this.openModal = true;
    this.mode = 'Habilidades';
    
  }

  async showMoves(pokemonDetails){
    await this.pokemonsService.getPokemonDetails(pokemonDetails)
      .then((data: any) => {
        this.CurrentPokemonDetails = data.moves;
      })
      .catch((error) => {
        this.toastr.error('Ocurrió un error al obtener los pokemons');
      });
    this.openModal = true;
    this.mode = 'Movimientos';
    
  }

  closedDetails(){
    this.CurrentPokemonDetails = [];
    this.openModal = !this.openModal;
  }
}
