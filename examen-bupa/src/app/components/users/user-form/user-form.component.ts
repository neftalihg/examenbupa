import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from 'src/app/services/users/users.service';
import { User } from 'src/app/interfaces/user';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() openModal: boolean;
  @Input() mode: string;
  @Input() currentUser: User;
  @Output() closedUserForm: EventEmitter<any>;
  @Output() userCreated: EventEmitter<boolean>;
  @Output() userUpdated: EventEmitter<boolean>;

  showUserForm: boolean;
  modeUserForm: string;
  
  @BlockUI() blockUI: NgBlockUI; 

  user: User = {
    first_name: '',
    last_name: '',
    email: '',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg'
  }

  constructor(private toastr: ToastrService,
    private usersService: UsersService) {
    this.closedUserForm = new EventEmitter();
    this.userCreated = new EventEmitter();
    this.userUpdated = new EventEmitter();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.showUserForm = this.openModal;
    this.modeUserForm = this.mode;
    console.log(this.mode);
    if(this.currentUser != null){
      this.user = this.currentUser;
    }
    else{
      this.user = {
        first_name: '',
        last_name: '',
        email: '',
        avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg'
      }
    }
  }

  save() {
    this.blockUI.start("Espere un momento");
    if (this.modeUserForm == "Nuevo Usuario") {
      this.usersService.createUser(this.user).subscribe((data: any) => {
        this.blockUI.stop();
        this.userCreated.emit();
      }, error => {
        this.blockUI.stop();
        this.toastr.error("Ocurrió un erro al crear el usuario");
      })
    }
    else if (this.modeUserForm == "Editar Usuario") {
      this.usersService.updateUser(this.user.id,this.user).subscribe((data: any) => {
        this.blockUI.stop();
        this.userUpdated.emit();
      }, error => {
        this.blockUI.stop();
        this.toastr.error("Ocurrió un erro al actualizar el usuario");
      })
    }

  }

  clean() {
    this.user.first_name = "";
    this.user.last_name = "";
    this.user.email = "";
  }

  close() {
    this.closedUserForm.emit();
  }
  ngOnInit(): void {
  }

}
