import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { User } from 'src/app/interfaces/user';
import { MatTableDataSource } from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { MatDialog } from '@angular/material';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  totalPages = null;
  Users: User[] = [];
  CurrentUser: User = null;

  @BlockUI() blockUI: NgBlockUI; 

  searchParameters = {
    FirstName: ''
  }

  openUserForm = false;
  modeUserForm = '';

  //Columnas y DataSource de Tabla
  displayedColumns: string[] = ['first_name', 'last_name', 'email', 'avatar', 'options'];
  dataSource = new MatTableDataSource<User>(this.Users);

  //Paginador
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private usersService: UsersService,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  async ngOnInit() {
    await this.getTotalPages();
    await this.getAllUsers();
    this.dataSource = new MatTableDataSource<User>(this.Users);
    this.dataSource.paginator = this.paginator;
  }

  async getTotalPages() {
    await this.usersService.getTotalPages()
      .then((data: any) => {
        this.totalPages = data.total_pages;
      })
      .catch((error) => {
        this.toastr.error('Ocurrió un error al obtener el número de páginas');
      });
  }

  async getAllUsers() {
    for (let page = 1; page <= this.totalPages; page++) {
      await this.usersService.getUserPerPage(page)
        .then((data: any) => {
          data.data.forEach(user => {
            this.Users.push(user);
          });
        })
        .catch((error) => {
          this.toastr.error("Ocurrió un error al obtener los usuarios de la página: "+ page)
        });

    }
  }

  deleteUser(userID) {
    this.blockUI.start("Espere un momento");
    this.usersService.deleteUser(userID).subscribe((data: any) => {
      this.blockUI.stop();
      this.toastr.success("Usuario eliminado exitosamente");
    }, error => {
      this.blockUI.stop();
      this.toastr.error("Ocurrió un error al eliminar el usuario", error);
    })
  }

  applyFilter() {
    this.dataSource.filterPredicate = (data, filter: string) => {
      let searchParameters = JSON.parse(filter);
      return !filter || (data.first_name.trim().toLowerCase().includes(searchParameters.firstname) 
      );
    };
    const filterValue = JSON.stringify(this.searchParameters);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteFilter() {
    this.searchParameters = {
      FirstName: ''
    }
    this.dataSource.filter = '';
  }

  closedUserForm(){
    this.openUserForm = !this.openUserForm;
  }

  userCreated(){
    this.openUserForm = !this.openUserForm;
    this.toastr.success("Se creó correctamente el usuario");
  }

  userUpdated(){
    this.openUserForm = !this.openUserForm;
    this.toastr.success("Se actualizó correctamente el usuario");
  }

  editUser(user){
    this.CurrentUser = user;
    this.openUserForm = true; 
    this.modeUserForm = 'Editar Usuario'
  }
}
