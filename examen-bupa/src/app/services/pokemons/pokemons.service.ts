import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonsService {

  constructor(private httpClient: HttpClient) { }

  endpoint = 'https://pokeapi.co/api/v2/';


  getPokemons() {
    return this.httpClient.get(`${this.endpoint}pokemon`).toPromise();
  }

  getPokemonDetails(pokemonURL: string){
    return this.httpClient.get(`${pokemonURL}`).toPromise();
  }

}
