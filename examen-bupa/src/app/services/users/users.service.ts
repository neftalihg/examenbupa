import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  endpoint = 'https://reqres.in/api/';
  constructor(private httpClient: HttpClient) { }

  getTotalPages() : Promise<any> {
    return this.httpClient.get(`${this.endpoint}users/`).toPromise();
  }

  getUser(idUser: number) {
    return this.httpClient.get(`${this.endpoint}users/${idUser}`);
  }

  getUserPerPage(pageNumber: number) : Promise<any>{
    return this.httpClient.get(`${this.endpoint}users/?page=${pageNumber}`).toPromise();
  }

  createUser(newUser:User) {
    return this.httpClient.post(`${this.endpoint}users/`,newUser);
  }

  updateUser(userID: Number,updateUser: User) {
    return this.httpClient.put(`${this.endpoint}users/${userID}`,updateUser);
  }

  deleteUser(userID) {
    return this.httpClient.delete(`${this.endpoint}users/${userID}`);
  }
}


